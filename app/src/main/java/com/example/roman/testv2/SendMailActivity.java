package com.example.roman.testv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SendMailActivity extends AppCompatActivity {

    private String username;
    private EditText subjectText;
    private EditText contentText;
    Button sendMailBtn;
    Button backBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);

        // logica para envio de mails
        subjectText = findViewById(R.id.subjectField);
        contentText = findViewById(R.id.contentField);
        sendMailBtn = findViewById(R.id.sendMailBtn);
        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        sendMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { "rkrivocheev015@alumnos.iua.edu.ar" });
                intent.putExtra(Intent.EXTRA_SUBJECT, subjectText.getText().toString());
                intent.putExtra(Intent.EXTRA_TEXT, contentText.getText().toString());

                startActivity(Intent.createChooser(intent, "Send Email"));
                Toast.makeText(getApplicationContext(), "Gracias por comunicarte con el desarrollador!", Toast.LENGTH_LONG).show();

                //intent = new Intent(SendMailActivity.this, MainActivity.class);
                //startActivity(intent);
            }
        });

        backBtn = findViewById(R.id.mainScreenBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SendMailActivity.this, MainActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });
    }
}
