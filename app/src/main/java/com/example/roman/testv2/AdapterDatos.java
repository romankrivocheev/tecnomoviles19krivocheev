package com.example.roman.testv2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.roman.testv2.MainActivity.date1;
import static com.example.roman.testv2.MainActivity.date1Day;
import static com.example.roman.testv2.MainActivity.stat1;
import static com.example.roman.testv2.MainActivity.temp1;

public class AdapterDatos extends RecyclerView.Adapter<AdapterDatos.ViewHolder> {


    ArrayList<String> listDatos;
    public AdapterDatos(ArrayList<String> listDatos) {
        setHasStableIds(true);

        this.listDatos = listDatos;
    }



    // Inflar el view
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,null,false);
        return new ViewHolder(view);
    }
    // comunicacion entre adaptador y esta clase
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Ver!
        holder.asignarDatos(listDatos.get(position), position);
    }

    @Override
    public int getItemCount() {
        return listDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dato1;
        TextView dato2;
        TextView dato3;
        TextView dato4;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            dato1 = itemView.findViewById(R.id.idDato1);
            dato2 = itemView.findViewById(R.id.idDato2);
            dato3 = itemView.findViewById(R.id.idDato3);
            dato4 = itemView.findViewById(R.id.idDato4);
        }


        public void asignarDatos(String datos, int position) {
            if(position == 0 || position == 4 || position ==8 || position == 12){
                if(position==0) {
                    if(date1Day == 2) {
                        dato1.setText("Lunes");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 2 ){
                        dato1.setText("Lunes");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 2 ){
                        dato1.setText("Lunes");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 2 ) {
                        dato1.setText("Lunes");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 2 ){
                        dato1.setText("Lunes");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    }

                }
                if(position==4) {
                    if(date1Day == 6) {
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 6 ){
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 6 ){
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 6 ) {
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 6 ){
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Viernes");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }

            }
            else if(position == 1 || position == 5 || position ==9 || position == 13){
                if(position==1) {
                    if(date1Day == 3) {
                        dato1.setText("Martes");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 3 ){
                        dato1.setText("Martes");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 3 ){
                        dato1.setText("Martes");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 3 ) {
                        dato1.setText("Martes");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 3 ){
                        dato1.setText("Martes");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Martes");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }
                if(position==5) {
                    if(date1Day == 7) {
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 7 ){
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 7 ){
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 7 ) {
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 7 ){
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Sábado");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }


            }
            else if(position == 2 || position == 6 || position ==10 || position == 14){
                if(position==2) {
                    if(date1Day == 4) {
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 4 ){
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 4 ){
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 4 ) {
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 4 ){
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Miercoles");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }
                if(position==6) {
                    if(date1Day == 1) {
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 1 ){
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 1 ){
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 1 ) {
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 1 ){
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Domingo");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }


            }
            else if(position == 3 || position == 7 || position ==11 || position == 15){
                if(position==3) {
                    if(date1Day == 5) {
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: "+date1);
                        dato3.setText("Temperatura: "+temp1);
                        dato4.setText("Estado: "+stat1);
                    } else if(MainActivity.date2Day == 5 ){
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: "+MainActivity.date2);
                        dato3.setText("Temperatura: "+MainActivity.temp2);
                        dato4.setText("Estado: "+MainActivity.stat2);
                    } else if(MainActivity.date3Day == 5 ){
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: "+MainActivity.date3);
                        dato3.setText("Temperatura: "+MainActivity.temp3);
                        dato4.setText("Estado: "+MainActivity.stat3);
                    } else if(MainActivity.date4Day == 5 ) {
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: " + MainActivity.date4);
                        dato3.setText("Temperatura: " + MainActivity.temp4);
                        dato4.setText("Estado: " + MainActivity.stat4);
                    }else if(MainActivity.date5Day == 5 ){
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: "+MainActivity.date5);
                        dato3.setText("Temperatura: "+MainActivity.temp5);
                        dato4.setText("Estado: "+MainActivity.stat5);
                    } else {
                        dato1.setText("Jueves");
                        dato2.setText("Fecha: "+"S/Info");
                        dato3.setText("Temperatura: "+"S/Info");
                        dato4.setText("Estado: "+"S/Info");
                    }
                }
            }

        }
    }
}
