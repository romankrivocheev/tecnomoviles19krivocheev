package com.example.roman.testv2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity
{
    private int STORAGE_PERMISSION_CODE = 1;
    private boolean isConectedToInternet;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.CAMERA
    };

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(int i=1 ; i<permissions.length ; i++) {
            if(grantResults[i] == -1) { // i = 2 location. i= 1 storage, 3 camara
                //Toast.makeText(this, "No ha otorgado el siguiente permiso:\n"+permissions[i]. + "\n\nVuelva a ingresar a la app!", Toast.LENGTH_LONG).show();
                switch(permissions[i]) {
                    case "android.permission.READ_EXTERNAL_STORAGE":
                        Toast.makeText(this, "No ha otorgado el el permiso de lectura/escritura de archivos!\n\nVuelva a ingresar a la app!", Toast.LENGTH_LONG).show();
                        break;
                    case "android.permission.ACCESS_FINE_LOCATION":
                        Toast.makeText(this, "No ha otorgado el el permiso de geolocalizacion!\n\nVuelva a ingresar a la app!", Toast.LENGTH_LONG).show();
                        break;
                    case "android.permission.CAMERA":
                        Toast.makeText(this, "No ha otorgado el el permiso de camara!\n\nVuelva a ingresar a la app!", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        }

    }
    Thread thread = new Thread(){
        @Override
        public void run() {
            try {
                Thread.sleep(2000);
                LoginActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Check if already logged in, if so, proceed to main screen
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (prefs.getBoolean("loggedIn", false)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("username", prefs.getString("username",null));
            //Toast.makeText(getApplicationContext(), "Bienvenido " + prefs.getString("username",null).substring(0, 1).toUpperCase() + prefs.getString("username",null).substring(1) + "!", Toast.LENGTH_LONG).show();
            startActivity(intent);
        }


        // Check network connectivity
        if(CheckNetwork.isInternetAvailable(LoginActivity.this)) //returns true if internet available
        {
            isConectedToInternet = true;
        }
        else
        {
            isConectedToInternet = false;
        }

        // Permissions check
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        final EditText txtUserName = (EditText)findViewById(R.id.username);
        final EditText txtPassword = (EditText)findViewById(R.id.password);
        Button btnLogin = (Button)findViewById(R.id.email_sign_in_button);
        Button btnRegister = (Button)findViewById(R.id.registerBtn);
        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                Toast.makeText(getApplicationContext(), "Complete los datos para un nuevo usuario!", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                if(isConectedToInternet) {
                    String username = txtUserName.getText().toString();
                    String password = txtPassword.getText().toString();
                    try{
                        if(username.length() > 0 && password.length() >0)
                        {
                            DBUserAdapter dbUser = new DBUserAdapter(LoginActivity.this);
                            dbUser.open();

                            if(dbUser.Login(username, password))
                            {
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putBoolean("loggedIn", true);
                                editor.putString("username",username);
                                editor.commit();

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("username", username);
                                Toast.makeText(getApplicationContext(), "Bienvenido " + username.substring(0, 1).toUpperCase() + username.substring(1) + "!", Toast.LENGTH_LONG).show();
                                startActivity(intent);

                            }else{
                                Toast.makeText(LoginActivity.this,"Usuario o contraseña invalidos. Reintente", Toast.LENGTH_LONG).show();

                            }

                            dbUser.close();
                        } else {
                            Toast.makeText(getApplicationContext(), "Ingrese usuario y contraseña ", Toast.LENGTH_LONG).show();

                        }

                    }catch(Exception e)
                    {
                        // Real error: e.getMessage()
                        Toast.makeText(LoginActivity.this,"Inicializando base de datos, vuelva a intentar..", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this,"No esta conectado a internet! Conectese, vuelva a ingresar a la app en unos instantes", Toast.LENGTH_LONG).show();
                    thread.start();
                }

            }

        });
    }
}