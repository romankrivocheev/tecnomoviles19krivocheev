package com.example.roman.testv2;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class DBUserAdapter
{
    public static final String KEY_ROWID = "_id";
    public static final String KEY_USERNAME= "username";
    public static final String KEY_PASSWORD = "password";
    private static final String TAG = "DBAdapter";

    private static final String DATABASE_NAME = "usersdb";
    private static final String DATABASE_TABLE = "users";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE =
            "create table users (_id integer primary key autoincrement, "
                    + "username text not null, "
                    + "name text not null, "
                    + "lastname text not null, "
                    + "age text not null, "
                    + "email text not null, "
                    + "weatherupdate text not null, "
                    + "measuresystem text not null, "
                    + "photourl text not null, "
                    + "password text not null);";

    private Context context = null;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBUserAdapter(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);


    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, "/sdcard/folderName/"+DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion
                    + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS users");
            onCreate(db);
        }
    }


    public void open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        if (prefs.getBoolean("firstTime", true)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", false);
            editor.commit();
            db.execSQL(DATABASE_CREATE);
        }

    }


    public void close()
    {
        DBHelper.close();
    }

    public long AddUser(String username, String password)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_USERNAME, username);
        initialValues.put(KEY_PASSWORD, password);
        initialValues.put("name", "none");
        initialValues.put("age", "none");
        initialValues.put("lastname", "none");
        initialValues.put("email", "none");
        initialValues.put("weatherupdate", "none");
        initialValues.put("measuresystem", "none");
        initialValues.put("photourl", "none");


        return db.insert(DATABASE_TABLE, null, initialValues);

    }
    public long SavePreferences(String username, String newName, String lastName, String age, String email, String celcius, String password, String weatherUpdateNumber, String imageUrl) {
        Cursor resultSet = db.rawQuery("select * from " + "users"+" where username =?", new String[]{username});
        resultSet.moveToFirst();
        String id = resultSet.getString(0);
        ContentValues values = new ContentValues();
        values.put("name", newName);
        values.put("lastname", lastName);
        values.put("age", age);
        values.put("email",email);
        values.put("measuresystem",celcius);
        values.put(KEY_PASSWORD,password);
        values.put("weatherupdate",weatherUpdateNumber);
        values.put("photourl",imageUrl);

        // updating row
         db.update(DATABASE_TABLE, values, "_id="+id, null);

        return 1;
    }
    public String[] LoadInfo(String username) {
        String[] userData = new String[10];
        Cursor resultSet = db.rawQuery("select * from " + "users"+" where username =?", new String[]{username});
        resultSet.moveToFirst();
        userData[0] = resultSet.getString(0);
        userData[1] = resultSet.getString(1);
        userData[2] = resultSet.getString(2);
        userData[3] = resultSet.getString(3);
        userData[4] = resultSet.getString(4);
        userData[5] = resultSet.getString(5);
        userData[6] = resultSet.getString(6);
        userData[7] = resultSet.getString(7);
        userData[8] = resultSet.getString(8);
        userData[9] = resultSet.getString(9);
        return userData;

    }

    public boolean Login(String username, String password) throws SQLException
    {

        // DB creation code:
        //db.execSQL(DATABASE_CREATE);
        Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE + " WHERE username=? AND password=?", new String[]{username,password});
        // Database wipe code:
        //db.execSQL("DROP TABLE IF EXISTS users");
        if (mCursor != null) {
            if(mCursor.getCount() > 0)
            {
                return true;
            }
        }
        return false;
    }

    public String DebugValues() {
        Cursor mCursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE + " WHERE username=? AND password=?", new String[]{"roman","11111"});

        return "" +mCursor.getCount();
    }

}
