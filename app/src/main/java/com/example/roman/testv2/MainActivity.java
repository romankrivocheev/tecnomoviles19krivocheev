package com.example.roman.testv2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    ArrayList<String> listDatos;
    RecyclerView recycler;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private String username;

    // Obtained data from API
    public  static String date1;
    public  static int date1Day;
    public  static String date2;
    public  static int date2Day;
    public  static String date3;
    public  static int date3Day;
    public  static String date4;
    public  static int date4Day;
    public  static String date5;
    public  static int date5Day;
    public  static String stat1;
    public  static String stat2;
    public  static String stat3;
    public  static String stat4;
    public  static String stat5;
    public  static String temp1;
    public  static String temp2;
    public  static String temp3;
    public  static String temp4;
    public  static String temp5;

    public Context context;

    private static final String NOTIFICATION_CHANNEL_ID = "my_channel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        // API Network logic
        //recycler = (RecyclerView) findViewById(R.id.recyclerId);
        //recycler.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        //LoadAPIClass loadAPIClass = new LoadAPIClass();
        //loadAPIClass.loadAPIData(getApplicationContext(), context, recycler);
        loadAPIData();

        //Set last time update
        Calendar calendar = Calendar.getInstance();
        TextView lastTimeUpdated = (TextView) findViewById(R.id.initBtn);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String lastUpdate = prefs.getString("lastUpdate", "S/información");
        lastTimeUpdated.setText("Ultima actualizacón:\n"+lastUpdate);

        //Obtain username
        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        // Drawer layout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    //Info de la API
    public void loadAPIData() {
        // ID Cordoba: 3860259
        // "name": "Cordoba",
        // "country": "AR"
        // Api key: 175cc976810bd911471a21d9d062bf39
        // URL 5 dias, Cordoba, Argentina
        // http:https://api.openweathermap.org/data/2.5/forecast?lat=-31.41&lon=-64.18&units=metric&appid=175cc976810bd911471a21d9d062bf39


        final String todayDataURL ="https://api.openweathermap.org/data/2.5/forecast?id=3860259&units=metric&appid=175cc976810bd911471a21d9d062bf39";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, todayDataURL,null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {


                    //Dates for the next 5 days
                    //Sunday = 1 in DAY_OF_WEEK
                    JSONArray array = (JSONArray) response.get("list");
                    JSONObject object = (JSONObject) array.get(0);
                    date1 = (String)object.get("dt_txt");
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date1Day = calendar.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    date2 = (String)object.get("dt_txt");
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date2Day = calendar2.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    date3 = (String)object.get("dt_txt");
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date3Day = calendar3.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    date4 = (String)object.get("dt_txt");
                    Calendar calendar4 = Calendar.getInstance();
                    calendar4.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date4Day = calendar4.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    date5 = (String)object.get("dt_txt");
                    Calendar calendar5 = Calendar.getInstance();
                    calendar5.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date5Day = calendar5.get(Calendar.DAY_OF_WEEK);

                    //Status
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(0);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat1 = (String) object.get("main");
                    if(stat1.equals("Clear")) {
                        stat1 = "Despejado";
                    }
                    if(stat1.equals("Clouds")) {
                        stat1 = "Nubosidad";
                    }
                    if(stat1.equals("Rain")) {
                        stat1 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat2 = (String) object.get("main");
                    if(stat2.equals("Clear")) {
                        stat2 = "Despejado";
                    }
                    if(stat2.equals("Clouds")) {
                        stat2 = "Nubosidad";
                    }
                    if(stat2.equals("Rain")) {
                        stat2 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat3 = (String) object.get("main");
                    if(stat3.equals("Clear")) {
                        stat3 = "Despejado";
                    }
                    if(stat3.equals("Clouds")) {
                        stat3 = "Nubosidad";
                    }
                    if(stat3.equals("Rain")) {
                        stat3 = "Lluvioso";
                    }
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat4 = (String) object.get("main");
                    if(stat4.equals("Clear")) {
                        stat4 = "Despejado";
                    }
                    if(stat4.equals("Clouds")) {
                        stat4 = "Nubosidad";
                    }
                    if(stat4.equals("Rain")) {
                        stat4 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat5 = (String) object.get("main");
                    if(stat5.equals("Clear")) {
                        stat5 = "Despejado";
                    }
                    if(stat5.equals("Clouds")) {
                        stat5 = "Nubosidad";
                    }
                    if(stat5.equals("Rain")) {
                        stat5 = "Lluvioso";
                    }

                    //Temperature
                    //response.get("list").get(0).get("main").get("temp")
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(0);
                    object = (JSONObject) object.get("main");
                    Double aux;
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp1 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp2 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp3 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp4 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp5 = Double.toString(aux);

                    //Load recycler view with data
                    recycler = (RecyclerView) findViewById(R.id.recyclerId);
                    recycler.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));


                    // Recycler view
                    listDatos = new ArrayList<String>();
                    for(int i=1 ; i<=7;i++) { // Paso la cantidad de valores del recycler que quiera agregar

                        listDatos.add(Integer.toString(i));
                    }

                    AdapterDatos adapter = new AdapterDatos(listDatos);
                    recycler.setAdapter(adapter);

                    //Save current time of update
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    editor.putString("lastUpdate",formatter.format(date));
                    editor.commit();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"No se pudo conectar con el servidor de datos!\n" +
                        "Compruebe su conexión a internet!", Toast.LENGTH_LONG).show();

            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(jor);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        //Toast.makeText(getApplicationContext(), "Pantalla pausada", Toast.LENGTH_LONG).show();
        //Setear una acción a la notificación:
        Intent intent2 = new Intent(this, MainActivity.class);
        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent2, 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Esto es para entrar a la app")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
        super.onPause();

    }
    @Override
    public void onResume() {
        //Toast.makeText(getApplicationContext(), "Pantalla resumida", Toast.LENGTH_LONG).show();
        super.onResume();
    }
    @Override
    public void onStart() {
        //Toast.makeText(getApplicationContext(), "Pantalla START", Toast.LENGTH_LONG).show();
        super.onStart();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if(id == R.id.home) {
            // Log out
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("loggedIn", false);
            editor.commit();
            // Return to log in screen
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if(id == R.id.settings) {

            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            intent.putExtra("username", username);
            startActivity(intent);
        }
        if(id == R.id.sendMail) {
            Intent intent = new Intent(MainActivity.this, SendMailActivity.class);
            intent.putExtra("username", username);
            startActivity(intent);
        }
        if(id == R.id.logOut) {
            // Close app without closing session
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
        return false;
    }
}
