package com.example.roman.testv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final EditText newUsername = (EditText)findViewById(R.id.newUsername);
        final EditText newPassword = (EditText)findViewById(R.id.newpassword1);
        final EditText newPassword2 = (EditText)findViewById(R.id.newpassword2);

        Button btnReturn = (Button)findViewById(R.id.return_log_in);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                Toast.makeText(getApplicationContext(), "No ha registrado un usuario nuevo", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

        Button btnRegister = (Button)findViewById(R.id.register_new_user);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;

                if(newUsername.getText().length() < 4) {
                    Toast.makeText(RegisterActivity.this,"El nombre del usuario es muy corto!", Toast.LENGTH_LONG).show();
                    error = true;
                }

                if(!newPassword.getText().toString().equals(newPassword2.getText().toString()) ) {
                    Toast.makeText(RegisterActivity.this,"Las contraseñas no coinciden!"+newPassword.getText()+" "+newPassword2.getText(), Toast.LENGTH_LONG).show();

                    error = true;
                }

                if(newPassword.getText().length() < 4 || newPassword2.getText().length() < 4 ) {
                    Toast.makeText(RegisterActivity.this,"La contraseña es muy corta!", Toast.LENGTH_LONG).show();
                    error = true;
                }
                if(newPassword.getText().length() < 1 || newPassword2.getText().length() < 1 || newUsername.getText().length() < 1) {
                    Toast.makeText(RegisterActivity.this,"No hay ingresado un nombre de usuario o contraseña!", Toast.LENGTH_LONG).show();
                    error = true;
                }

                if(!error) {
                    DBUserAdapter dbUser = new DBUserAdapter(RegisterActivity.this);
                    dbUser.open();

                    dbUser.AddUser(newUsername.getText().toString(),newPassword.getText().toString());
                    dbUser.close();

                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    Toast.makeText(getApplicationContext(), "Usuario " + newUsername.getText() + " registrado con exito!", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }
            }
        });
    }
}
