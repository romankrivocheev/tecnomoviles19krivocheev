package com.example.roman.testv2;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class LoadAPIClass {
    RecyclerView recyclerMain;
    Context mainActivityContext;
    Context appContext;

    // Obtained data from API
    public  static String date1;
    public  static int date1Day;
    public  static String date2;
    public  static int date2Day;
    public  static String date3;
    public  static int date3Day;
    public  static String date4;
    public  static int date4Day;
    public  static String date5;
    public  static int date5Day;
    public  static String stat1;
    public  static String stat2;
    public  static String stat3;
    public  static String stat4;
    public  static String stat5;
    public  static String temp1;
    public  static String temp2;
    public  static String temp3;
    public  static String temp4;
    public  static String temp5;


    //Info de la API
    public void loadAPIData(Context contextApp, Context contextMain, RecyclerView recycler) {
        // ID Cordoba: 3860259
        // "name": "Cordoba",
        // "country": "AR"
        // Api key: 175cc976810bd911471a21d9d062bf39
        // URL 5 dias, Cordoba, Argentina
        // http:https://api.openweathermap.org/data/2.5/forecast?lat=-31.41&lon=-64.18&units=metric&appid=175cc976810bd911471a21d9d062bf39
        appContext = contextApp;
        mainActivityContext = contextMain;
        recyclerMain = recycler;
        final String todayDataURL ="https://api.openweathermap.org/data/2.5/forecast?id=3860259&units=metric&appid=175cc976810bd911471a21d9d062bf39";
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, todayDataURL,null,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    //Dates for the next 5 days
                    //Sunday = 1 in DAY_OF_WEEK
                    JSONArray array = (JSONArray) response.get("list");
                    JSONObject object = (JSONObject) array.get(0);
                    date1 = (String)object.get("dt_txt");
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date1Day = calendar.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    date2 = (String)object.get("dt_txt");
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date2Day = calendar2.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    date3 = (String)object.get("dt_txt");
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date3Day = calendar3.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    date4 = (String)object.get("dt_txt");
                    Calendar calendar4 = Calendar.getInstance();
                    calendar4.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date4Day = calendar4.get(Calendar.DAY_OF_WEEK);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    date5 = (String)object.get("dt_txt");
                    Calendar calendar5 = Calendar.getInstance();
                    calendar5.setTimeInMillis((Integer)object.get("dt")*1000L);
                    date5Day = calendar5.get(Calendar.DAY_OF_WEEK);

                    //Status
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(0);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat1 = (String) object.get("main");
                    if(stat1.equals("Clear")) {
                        stat1 = "Despejado";
                    }
                    if(stat1.equals("Clouds")) {
                        stat1 = "Nubosidad";
                    }
                    if(stat1.equals("Rain")) {
                        stat1 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat2 = (String) object.get("main");
                    if(stat2.equals("Clear")) {
                        stat2 = "Despejado";
                    }
                    if(stat2.equals("Clouds")) {
                        stat2 = "Nubosidad";
                    }
                    if(stat2.equals("Rain")) {
                        stat2 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat3 = (String) object.get("main");
                    if(stat3.equals("Clear")) {
                        stat3 = "Despejado";
                    }
                    if(stat3.equals("Clouds")) {
                        stat3 = "Nubosidad";
                    }
                    if(stat3.equals("Rain")) {
                        stat3 = "Lluvioso";
                    }
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat4 = (String) object.get("main");
                    if(stat4.equals("Clear")) {
                        stat4 = "Despejado";
                    }
                    if(stat4.equals("Clouds")) {
                        stat4 = "Nubosidad";
                    }
                    if(stat4.equals("Rain")) {
                        stat4 = "Lluvioso";
                    }

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    array = (JSONArray)object.get("weather");
                    object = (JSONObject) array.get(0);
                    stat5 = (String) object.get("main");
                    if(stat5.equals("Clear")) {
                        stat5 = "Despejado";
                    }
                    if(stat5.equals("Clouds")) {
                        stat5 = "Nubosidad";
                    }
                    if(stat5.equals("Rain")) {
                        stat5 = "Lluvioso";
                    }

                    //Temperature
                    //response.get("list").get(0).get("main").get("temp")
                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(0);
                    object = (JSONObject) object.get("main");
                    Double aux;
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp1 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(8);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp2 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(16);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp3 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(24);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp4 = Double.toString(aux);

                    array = (JSONArray) response.get("list");
                    object = (JSONObject) array.get(32);
                    object = (JSONObject) object.get("main");
                    if(object.get("temp") instanceof Integer) {
                        aux = new Double( (Integer) object.get("temp"));
                    } else {
                        aux = (Double)object.get("temp");
                    }
                    temp5 = Double.toString(aux);

                    //Load recycler view with data

                    // Recycler view
                    ArrayList<String> listDatos = new ArrayList<String>();
                    for(int i=1 ; i<=7;i++) { // Paso la cantidad de valores del recycler que quiera agregar

                        listDatos.add(Integer.toString(i));
                    }

                    AdapterDatos adapter = new AdapterDatos(listDatos);
                    recyclerMain.setAdapter(adapter);

                    //Save current time of update
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
                    SharedPreferences.Editor editor = prefs.edit();
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    editor.putString("lastUpdate",formatter.format(date));
                    editor.commit();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mainActivityContext,"No se pudo conectar con el servidor de datos!\n" +
                        "Compruebe su conexión a internet!", Toast.LENGTH_LONG).show();

            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(appContext);
        queue.add(jor);

    }
}
