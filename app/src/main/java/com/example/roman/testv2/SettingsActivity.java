package com.example.roman.testv2;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class SettingsActivity extends AppCompatActivity {
    private ImageButton imageButton;
    private Button backBtn;
    private Button savePrefsChangesBtn;
    private final int GALLERY_REQUEST_CODE = 10;
    private String username;
    private String imageUrl;
    private static final String NOTIFICATION_CHANNEL_ID = "my_channel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //manageNotifications();

        //Obtain current user
        Intent intent = getIntent();
        username = intent.getStringExtra("username");

        //Poblate settings data
        try {
            fillData();
        }catch(Exception e) {
            Toast.makeText(getApplicationContext(), "Error inesperado! Reinicie la app por favor", Toast.LENGTH_LONG).show();
        }
        //Save prefs
        savePrefsChangesBtn = findViewById(R.id.savePrefsChanges);
        savePrefsChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Save data in database
                DBUserAdapter dbUser = new DBUserAdapter(SettingsActivity.this);
                dbUser.open();

                EditText name = (EditText) findViewById(R.id.editText4);
                EditText lastName = (EditText) findViewById(R.id.editText5);
                EditText age= (EditText) findViewById(R.id.editText6);
                EditText email = (EditText) findViewById(R.id.editText7);
                EditText password = (EditText) findViewById(R.id.editText);
                EditText password2 = (EditText) findViewById(R.id.editText2);
                if(!password.getText().toString().equals(password2.getText().toString())) {
                    dbUser.close();
                    Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden! Verifique", Toast.LENGTH_SHORT).show();
                    return;
                }
                Switch s = (Switch) findViewById(R.id.switch1);
                String celcius;
                if(s.isChecked()) {
                    celcius = "yes";
                } else {
                    celcius = "no";
                }
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupSettings);
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(selectedId);

                String weatherUpdateNumber = "0";
                if(radioButton.getText().equals("30min")) {
                    weatherUpdateNumber = "1";
                }else if(radioButton.getText().equals("1hr")) {
                    weatherUpdateNumber = "2";
                }else if(radioButton.getText().equals("24hs")) {
                    weatherUpdateNumber = "3";
                }

                dbUser.SavePreferences(username,name.getText().toString(), lastName.getText().toString(),
                        age.getText().toString(),email.getText().toString(),celcius,password.getText().toString(),
                        weatherUpdateNumber, imageUrl);
                Toast.makeText(getApplicationContext(), "Se han guardado los cambios con exito!", Toast.LENGTH_SHORT).show();

                dbUser.close();
            }
        });
        //Return btn
        backBtn = findViewById(R.id.mainScreenBtn5);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        // Image clicked for taking photo and save
        imageButton = findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Create an Intent with action as ACTION_PICK
                Intent intent= new Intent(Intent.ACTION_PICK);
                // Sets the type as image/*. This ensures only components of type image are selected
                intent.setType("image/*");
                //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                // Launching the Intent
                startActivityForResult(intent,GALLERY_REQUEST_CODE);
            }
        });
    }

    private void manageNotifications() {

        Context context = getApplicationContext();

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(getApplicationContext().getString(R.string.app_name))
                    .setContentText("Mensaje de notificacion!")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            //Crear el Notification Channel para versiones de android posteriores a API 26.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                CharSequence name = context.getString(R.string.app_name);
                String description = "Esta es una descripcion!";
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setDescription(description);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(1, builder.build());
    }

    public void fillData() {
        // Obtain data from database
        DBUserAdapter dbUser = new DBUserAdapter(SettingsActivity.this);
        dbUser.open();
        String[] userData = dbUser.LoadInfo(username);
        dbUser.close();
        // Fill values in UI
        //username
        EditText usernameText = (EditText) findViewById(R.id.EditText);
        usernameText.setText(userData[1]);
        usernameText.setEnabled(false);
        //password
        EditText passText = (EditText) findViewById(R.id.editText);
        passText.setText(userData[9]);
        EditText passText2 = (EditText) findViewById(R.id.editText2);
        passText2.setText(userData[9]);
        //name
        EditText name = (EditText) findViewById(R.id.editText4);
        name.setText(userData[2]);
        //lastName
        EditText lastNameText = (EditText) findViewById(R.id.editText5);
        lastNameText.setText(userData[3]);
        //age
        EditText ageText = (EditText) findViewById(R.id.editText6);
        ageText.setText(userData[4]);
        //email
        EditText emailText = (EditText) findViewById(R.id.editText7);
        emailText.setText(userData[5]);
        //Celcius?
        Switch s = (Switch) findViewById(R.id.switch1);
        if(userData[7].equals("yes")) {
            s.setChecked(true);
        } else {
            s.setChecked(false);
        }
        //Sync radioButton
        switch(userData[6]) {
            case "0":
                RadioButton radioButton = findViewById(R.id.radioButton);
                radioButton.setChecked(true);
                break;
            case "2":
                RadioButton radioButton2 = findViewById(R.id.radioButton2);
                radioButton2.setChecked(true);
                break;
            case "3":
                RadioButton radioButton3 = findViewById(R.id.radioButton3);
                radioButton3.setChecked(true);
                break;
        }

        //Set imageButton image
        imageUrl = userData[8];
        File imgFile = new  File(userData[8]);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageButton myImage = (ImageButton) findViewById(R.id.imageButton);
            try {
                myImage.setImageBitmap(modifyOrientation(myBitmap,userData[8]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK) switch (requestCode) {
            case GALLERY_REQUEST_CODE:
                //data.getData return the content URI for the selected Image
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                //Get the column index of MediaStore.Images.Media.DATA
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //Gets the String value in the column
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                setImageButton(imgDecodableString);
                imageUrl = imgDecodableString;
                break;

        }
    }
    public void setImageButton(String imgDecodableString) {
        try {
            imageButton.setImageBitmap(modifyOrientation(BitmapFactory.decodeFile(imgDecodableString), imgDecodableString));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}
